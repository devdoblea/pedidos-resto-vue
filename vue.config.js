module.exports = {
  devServer: {
    proxy: 'api.papapollo.local/',
  },
  configureWebpack: {
    devServer: {
      headers: { "Access-Control-Allow-Origin": "api.papapollo.local/" }
    }
  },
  /*publicPath: process.env.NODE_ENV === "production" ? "/pedidos-resto-vue" : "/"*/
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/'
};
