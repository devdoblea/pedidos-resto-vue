// window.onscroll = () =>{
//    const navbar = document.querySelector('.header .flex .navbar');
//    const profile = document.querySelector('.header .flex .profile');
//    profile.classList.remove('active');
//    navbar.classList.remove('active');
// }

// window.onload = fadeOut;
window.onload = () => {
   const navbar = document.querySelector('.header .flex .navbar');
   const profile = document.querySelector('.header .flex .profile');
   const userBtn = document.querySelector('#user-btn');
   const menuBtn = document.querySelector('#menu-btn');
   const linkBtn = document.querySelectorAll('.linkBtn');
   // boton del menu lateral para desplegar el navbar en pantallas pequeñas
   menuBtn.onclick = () => {
      navbar.classList.toggle('active');
      // profile.classList.remove('active');
   }

   // boton del menu de usuarios
   userBtn.onclick = () => {
      // profile.classList.toggle('active');
      navbar.classList.remove('active');
   }

   // para los botones del navbar
   linkBtn.forEach(function(e, i) {
     e.addEventListener('click', function(){
       navbar.classList.remove('active');
     });
   });

   const loader = () => {
      const spinLoader = document.querySelector('.loader');
      spinLoader.style.display = 'none';
   }

   const fadeOut = () => {
      setInterval(loader, 2000);
   }

   // para cargar el spinloader
   fadeOut();
}