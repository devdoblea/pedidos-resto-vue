import { createStore } from 'vuex'

// Import axios to make HTTP requests
import axios from 'axios'

export default createStore({
  state: {
    token: [],
    pedidos: [],
    productos: [],
  },
  getters: {
    getPedidos: (state) => state.pedidos,
    getProductos: (state) => state.productos
  },
  actions: {
    async fetchPedidos({ commit },  params ) {
      try {
        const data = await axios.get('http://api.papapollo.local/api/pedidos/porpagina/'+params.porPagina)
          commit('SET_PEDIDOS', data.data)
      }
      catch (error) {
        // alert(error)
        console.log(error)
      }
    },
    async fetchProductos({ commit },  params ) {
      try {
        const data = await axios.get('http://api.papapollo.local/api/productos/porpagina/'+params.porPagina)
          commit('SET_PRODUCTOS', data.data)
      }
      catch (error) {
        // alert(error)
        console.log(error)
      }
    }
  },
  mutations: {
    SET_PEDIDOS(state, pedidos) {
      state.pedidos = pedidos
    },
    SET_PRODUCTOS(state, productos) {
      state.productos = productos
    }
  },
  modules: {
  }
})
